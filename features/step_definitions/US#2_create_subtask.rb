require 'watir-webdriver'

Given (/^create new task as a prerequisite$/) do
  $valid_task_name = "1111"
  $browser.text_field(:name, "new_task").set($valid_task_name)
  $browser.send_keys :enter
end

Then (/^user must be able to see a button labeled as 'Manage Subtasks'$/) do
  if $browser.button(:text, /Manage Subtasks/).exists?
    puts "Manage Subtasks button exists"
  else
    puts "Manage Subtasks button does not exists"
  end
  $browser.quit
end

Then (/^user must see the number of subtasks created for that task on 'Manage Subtasks'$/) do
  if $browser.button(:text, /\d/).exists?
    puts "number of subtasks displayed"
  else
    puts "number of subtasks not displayed"
  end
  $browser.quit
end

When (/^user clicks on 'Manage Subtasks' button$/) do
  $browser.button(:class, 'btn btn-xs btn-primary ng-binding').click
end

Then (/^user must see a modal dialog$/) do
  if $browser.div(:class, 'modal-dialog').exists?
    puts "modal-dialog exists"
  else
    puts "modal-dialog doesn't exists"
  end
  $browser.quit
end
Then(/^user must see a textfield for SubTask description and SubTask due date \(MM\/dd\/yyyy format\)$/) do
  if $browser.text_field(:name, "new_sub_task").exists? && $browser.text_field(:name, "due_date").exists?
    puts 'Text_field for description and due date exists'
  elsif !$browser.text_field(:name, "new_sub_task").exists? && $browser.text_field(:name, "due_date").exists?
    puts 'Text_field for description only exists'
  elsif $browser.text_field(:name, "new_sub_task").exists? && !$browser.text_field(:name, "due_date").exists?
    puts 'Text_field for due date only exists'
  else
    puts 'None of the text_fields exists'
  end
  $browser.quit
end

And(/^user insert valid SubTask description and SubTask due date in text fields$/) do
  $valid_subtask_name = "1111"
  $browser.text_field(:name, 'new_sub_task').set($valid_subtask_name)
  $browser.text_field(:name, 'due_date').set("1/1/1990")
  $count = $browser.trs(:class, 'ng-scope').size
end

When(/^user clicks on 'Add' button$/) do
  $browser.button(:id, "add-subtask").click
end

Then(/^user must see a new SubTask added on the bottom of a modal dialog$/) do
  if $browser.text.include?($valid_subtask_name) && $count != $browser.trs(:class, 'ng-scope').size
    puts "Subtask added"
  else
    puts "Subtask not added"
  end
  $browser.quit
end

And(/^SubTask due date and description are emptied$/) do
  $browser.text_field(:name, 'new_sub_task').set("")
  $browser.text_field(:name, 'due_date').set("")
  $count = $browser.trs(:class, 'ng-scope').size
end

And (/^user insert valid SubTask description and invalid SubTask due date in text fields$/) do
  $valid_subtask_name = "1111"
  $browser.text_field(:name, 'new_sub_task').set($valid_subtask_name)
  $browser.text_field(:name, 'due_date').set("asdfasdf")
  $count = $browser.trs(:class, 'ng-scope').size
end

And (/^user insert invalid SubTask description in text fields$/) do
  $invalid_subtask_name = "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
  $browser.text_field(:name, 'new_sub_task').set($invalid_subtask_name)
  $count = $browser.trs(:class, 'ng-scope').size
end

Then(/^user must not see a new SubTask added on the bottom of a modal dialog$/) do
  if $count != $browser.trs(:class, 'ng-scope').size
    puts "Subtask added"
  else
    puts "Subtask not added"
  end
  $browser.quit
end

Then (/^user should be able to see a read only field with the task ID and the task description$/) do
  if $browser.h3(:text, /\d{5}/).exists?
    puts "5 digits task ID exists"
  else
    puts "task ID does not exists"
  end
  if $browser.text.include?($valid_task_name)
    puts "task name exists on modal dialog"
  else
    puts "task name does not exists on modal dialog"
  end
  $browser.quit
end

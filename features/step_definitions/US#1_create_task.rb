require 'watir-webdriver'


Given (/^user is on qa\-test\.avenuecode\.com link and click sign in button/) do
  $browser = Watir::Browser.new
  $browser.goto('http://qa-test.avenuecode.com')
  $browser.link(:text => "Sign In").when_present.click

end

Given (/^user is logging in$/) do
  $browser.text_field(:name, "user[email]").set("wntmddussla@hotmail.com")
  $browser.text_field(:name, "user[password]").set("12341234")
  $browser.button(:name, "commit").click
end

Given (/^user is on the 'My Tasks page'$/) do
  if $browser.link(:text => "My Tasks").exists?
    $browser.link(:text => "My Tasks").click
  else
    puts "My Tasks tab doesnt exist"
  end
end

When (/^'My Tasks' tab exists, then click it$/) do
  if $browser.link(:text => "My Tasks").exists?
    $browser.link(:text => "My Tasks").click
  else
    puts "My Tasks tab doesnt exist"
  end
end

And (/^user types valid size task name and press enter$/) do
  $valid_task_name = "1111"
  $browser.text_field(:name, "new_task").set($valid_task_name)
  $browser.send_keys :enter
end

And (/^user types tasks name that is less than 3 characters and clicks add task button$/) do
  $short_task_name = "1"
  $browser.text_field(:name, "new_task").set($short_task_name)
  $browser.span(:class, "input-group-addon glyphicon glyphicon-plus").click
end

And (/^user types tasks name that is more than 250 characters and clicks add task button$/) do
  $long_task_name = "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
  $browser.text_field(:name, "new_task").set($long_task_name)
  $browser.span(:class => "input-group-addon glyphicon glyphicon-plus").click
end

Then (/^short name task should not be added on the Todo list$/) do
  if $browser.text.include?($short_task_name)
    puts "Task has been added"
  else
    puts "Task is not added"
  end
  $browser.quit
end

Then (/^task should be added on the Todo list$/) do
  if $browser.text.include?($valid_task_name)
    puts "Task has been added"
  else
    puts "Task is not added"
  end
  $browser.quit
end

Then (/^long name task should not be added on the Todo list$/) do
  if $browser.text.include?($long_task_name)
    puts "Task has been added"
  else
    puts "Task is not added"
  end
  $browser.quit
end

Then (/^the user should see a message on the top part saying this list belongs to logged user$/) do
  if $browser.text.include?("Hey Seung Yeon Joo, this is your todo list for today:")
    puts "there is a welcome message"
  else
    puts "there is not a welcome message with name"
  end
  $browser.quit
end

Feature:
  As a ToDo App user
  I should be able to create a subtask
  So I can break down my tasks in smaller pieces

  Background:
    Given user is on qa-test.avenuecode.com link and click sign in button
    Given user is logging in
    Given user is on the 'My Tasks page'
    Given create new task as a prerequisite

    Scenario: The user should see a button labeled as ‘Manage Subtasks’ button
      Then user must be able to see a button labeled as 'Manage Subtasks'

    Scenario: 'Manage Subtasks' button should display the number of subtasks created for that task
      Then user must see the number of subtasks created for that task on 'Manage Subtasks'

    Scenario: Clicking 'Manage Subtasks' button should open up a modal dialog
      When user clicks on 'Manage Subtasks' button
      Then user must see a modal dialog

    Scenario: There should be a form so you can enter the SubTask Description (250 characters) and SubTask due date (MM/dd/yyyy format)
      When user clicks on 'Manage Subtasks' button
      Then user must see a textfield for SubTask description and SubTask due date (MM/dd/yyyy format)

    Scenario: User should be able to add a new subtask with valid SubTask description and Subtask due date inserted
      When user clicks on 'Manage Subtasks' button
      And user insert valid SubTask description and SubTask due date in text fields
      And user clicks on 'Add' button
      Then user must see a new SubTask added on the bottom of a modal dialog

    Scenario: User should not be able to add a new subtask with no SubTask description and no SubTask due date inserted
      When user clicks on 'Manage Subtasks' button
      And SubTask due date and description are emptied
      And user clicks on 'Add' button
      Then user must not see a new SubTask added on the bottom of a modal dialog

    Scenario: Modal dialog should have a read only field with the task ID and the task description
      When user clicks on 'Manage Subtasks' button
      Then user should be able to see a read only field with the task ID and the task description

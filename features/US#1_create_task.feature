Feature:
  As a ToDo App user, I should be able to create a task So I can manage my tasks

  Background:
    Given user is on qa-test.avenuecode.com link and click sign in button
    Given user is logging in

    Scenario: Click 'My Tasks' button on the navigation bar to see list of tasks created if it exists
      When 'My Tasks' tab exists, then click it
      Then the user should see a message on the top part saying this list belongs to logged user

    Scenario: User should be able to add valid task with enter key
      When 'My Tasks' tab exists, then click it
      And user types valid size task name and press enter
      Then task should be added on the Todo list

    Scenario: User should not be able to add new tasks when task name is less than 3 characters
      When 'My Tasks' tab exists, then click it
      And user types tasks name that is less than 3 characters and clicks add task button
      Then short name task should not be added on the Todo list

    Scenario: User should not be able to add new tasks when task name is more than 250 characters
      When 'My Tasks' tab exists, then click it
      And user types tasks name that is more than 250 characters and clicks add task button
      Then long name task should not be added on the Todo list
